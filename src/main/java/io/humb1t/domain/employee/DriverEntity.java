package io.humb1t.domain.employee;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "drivers")
@NamedQuery(name = "qwe",
        query = "SELECT dr " +
                "FROM DriverEntity dr " +
                "JOIN dr.department d " +
                "JOIN dr.man m " +
                "WHERE d.id = :deptId " +
                "GROUP BY dr.id, dr.employmentDate, dr.dismissDate " +
                "HAVING m.birthDate = MAX(m.birthDate)"
)
public class DriverEntity extends EmployeeEntity {

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
            name = "driver_categories",
            joinColumns = @JoinColumn(name = "driver_id")
    )
    @Column(name = "category")
    private List<DriverCategory> categories = new ArrayList<>();

    public DriverEntity() {
    }

    public DriverEntity(Long id) {
        super(id);
    }

    public DriverEntity(ManEntity man, DepartmentEntity department, Date employmentDate, Date dismissDate, List<DriverCategory> categories) {
        super(man, department, employmentDate, dismissDate);
        this.categories = categories;
    }

    public DriverEntity(Long id, ManEntity man, DepartmentEntity department, Date employmentDate, Date dismissDate, List<DriverCategory> categories) {
        super(id, man, department, employmentDate, dismissDate);
        this.categories = categories;
    }

    public List<DriverCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<DriverCategory> categories) {
        this.categories = categories;
    }

    @Override
    public boolean equals(Object o) {
        return this == o || o instanceof DriverEntity && super.equals(o);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode());
    }

    @Override
    public String toString() {
        return "DriverEntity{" +
                "categories=" + categories +
                "} " + super.toString();
    }
}
