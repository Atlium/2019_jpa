package io.humb1t.domain.employee;

import io.humb1t.utils.date.DateUtils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@SequenceGenerator(name = "jpaSequence", sequenceName = "jpa_sequence", allocationSize = 1)
@NamedQuery(name = "DepartmentEntity.findByManBirthdays",
        query = "" +
                "SELECT d " +
                "FROM EmployeeEntity e " +
                "JOIN e.department d " +
                "JOIN e.man m " +
                "WHERE d = e.department " +
                "AND m.birthDate BETWEEN :fromDate AND :toDate " +
                "GROUP BY d " +
                "HAVING COUNT(e) >= 3 " +
                "ORDER BY d.deptName"
)
@Table(name = "departments")
public class DepartmentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "jpaSequence")
    private Long id;

    @Column(name = "dept_name")
    private String deptName;

    @Column(name = "foundation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date foundationDate;

    @OneToMany(mappedBy = "man", fetch = FetchType.EAGER)
    private List<EmployeeEntity> employees = new ArrayList<>();

    public DepartmentEntity() {
    }

    public DepartmentEntity(Long id) {
        this.id = id;
    }

    public DepartmentEntity(String deptName, Date foundationDate) {
        this.deptName = deptName;
        this.foundationDate = foundationDate;
    }

    public DepartmentEntity(Long id, String deptName, Date foundationDate) {
        this.id = id;
        this.deptName = deptName;
        this.foundationDate = foundationDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Date getFoundationDate() {
        return foundationDate;
    }

    public void setFoundationDate(Date foundationDate) {
        this.foundationDate = foundationDate;
    }

    public List<EmployeeEntity> getEmployees() {
        return employees;
    }

    public void setEmployees(List<EmployeeEntity> employees) {
        this.employees = employees;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DepartmentEntity)) return false;
        DepartmentEntity that = (DepartmentEntity) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", deptName='" + deptName + '\'' +
                ", foundationDate=" + DateUtils.formatToTimestamp(foundationDate) +
                '}';
    }
}
