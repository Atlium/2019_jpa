package io.humb1t.domain.employee;

import io.humb1t.domain.activity.CourseEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@SequenceGenerator(name = "jpaSequence", sequenceName = "jpa_sequence", allocationSize = 1)
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "employees")
public class EmployeeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "jpaSequence")
    private Long id;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "man_id")
    private ManEntity man;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "department_id")
    private DepartmentEntity department;

    @Column(name = "employment_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date employmentDate;

    @Column(name = "dismiss_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dismissDate;

    @ManyToMany
    @JoinTable(
            name = "employee_courses",
            joinColumns = @JoinColumn(name = "employee_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "course_id", referencedColumnName = "id")
    )
    private Set<CourseEntity> courses = new HashSet<>();

    public EmployeeEntity() {
    }

    public EmployeeEntity(Long id) {
        this.id = id;
    }

    public EmployeeEntity(ManEntity man, DepartmentEntity department, Date employmentDate, Date dismissDate) {
        this.man = man;
        this.department = department;
        this.employmentDate = employmentDate;
        this.dismissDate = dismissDate;
    }

    public EmployeeEntity(Long id, ManEntity man, DepartmentEntity department, Date employmentDate, Date dismissDate) {
        this.id = id;
        this.man = man;
        this.department = department;
        this.employmentDate = employmentDate;
        this.dismissDate = dismissDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ManEntity getMan() {
        return man;
    }

    public void setMan(ManEntity man) {
        this.man = man;
    }

    public DepartmentEntity getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentEntity department) {
        this.department = department;
    }

    public Date getEmploymentDate() {
        return employmentDate;
    }

    public void setEmploymentDate(Date employmentDate) {
        this.employmentDate = employmentDate;
    }

    public Date getDismissDate() {
        return dismissDate;
    }

    public void setDismissDate(Date dismissDate) {
        this.dismissDate = dismissDate;
    }

    public Set<CourseEntity> getCourses() {
        return courses;
    }

    public void setCourses(Set<CourseEntity> courses) {
        this.courses = courses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EmployeeEntity)) return false;
        EmployeeEntity that = (EmployeeEntity) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "EmployeeEntity{" +
                "id=" + id +
                ", man=" + man +
                ", department=" + department +
                ", employmentDate=" + employmentDate +
                ", dismissDate=" + dismissDate +
                '}';
    }
}
