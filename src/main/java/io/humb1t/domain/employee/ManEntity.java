package io.humb1t.domain.employee;

import io.humb1t.utils.date.DateUtils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@NamedQuery(name = "ManEntity.findAllUnemployed",
        query = "SELECT m " +
                "FROM ManEntity m LEFT JOIN m.employees e " +
                "GROUP BY m.id " +
                "HAVING COUNT(e) = 0 " +
                "ORDER BY m.lastName, m.firstName")
@SequenceGenerator(name = "jpaSequence", sequenceName = "jpa_sequence", allocationSize = 1)
@Table(name = "mans")
public class ManEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "jpaSequence")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "birth_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date birthDate;

    @OneToMany(mappedBy = "man")
    private List<EmployeeEntity> employees = new ArrayList<>();

    public ManEntity() {
    }

    public ManEntity(Long id) {
        this.id = id;
    }

    public ManEntity(Long id, String firstName, String lastName, Date birthDate) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {

        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public List<EmployeeEntity> getEmployees() {
        return employees;
    }

    public void setEmployees(List<EmployeeEntity> employees) {
        this.employees = employees;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ManEntity)) return false;
        ManEntity manEntity = (ManEntity) o;
        return Objects.equals(getId(), manEntity.getId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "ManEntity{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + DateUtils.formatToTimestamp(birthDate) +
                '}';
    }
}
