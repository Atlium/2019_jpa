package io.humb1t.domain.shipment;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@SequenceGenerator(name = "jpaSequence", sequenceName = "jpa_sequence", allocationSize = 1)
@NamedQuery(name = "ShipmentOrderEntity.findOrdersWithShipmentsCount",
        query = "SELECT o " +
                "FROM ShipmentEntity s " +
                "JOIN s.shipmentOrder o " +
                "GROUP BY o " +
                "HAVING COUNT(s) = :shipmentCount " +
                "ORDER BY o.address"
)
@Table(name = "shipment_orders")
public class ShipmentOrderEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "jpaSequence")
    private Long id;

    @Column
    private String address;

    @Column(name = "shipment_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date shipmentDate;

    public ShipmentOrderEntity() {
    }

    public ShipmentOrderEntity(Long id) {
        this.id = id;
    }

    public ShipmentOrderEntity(String address, Date shipmentDate) {
        this.address = address;
        this.shipmentDate = shipmentDate;
    }

    public ShipmentOrderEntity(Long id, String address, Date shipmentDate) {
        this.id = id;
        this.address = address;
        this.shipmentDate = shipmentDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getShipmentDate() {
        return shipmentDate;
    }

    public void setShipmentDate(Date shipmentDate) {
        this.shipmentDate = shipmentDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ShipmentOrderEntity)) return false;
        ShipmentOrderEntity shipment = (ShipmentOrderEntity) o;
        return Objects.equals(getId(), shipment.getId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "ShipmentOrder{" +
                "id=" + id +
                ", address='" + address + '\'' +
                ", shipmentDate=" + shipmentDate +
                '}';
    }
}
