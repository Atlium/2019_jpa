package io.humb1t.domain.shipment;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ShipmentId implements Serializable {

    private static final long serialVersionUID = 451363326069869324L;

    private Long shipmentOrderId;

    private Long driverId;

    private Long lorryId;

    public ShipmentId() {
    }

    public ShipmentId(Long shipmentOrderId, Long driverId, Long lorryId) {
        this.shipmentOrderId = shipmentOrderId;
        this.driverId = driverId;
        this.lorryId = lorryId;
    }

    public Long getShipmentOrderId() {
        return shipmentOrderId;
    }

    public void setShipmentOrderId(Long shipmentOrderId) {
        this.shipmentOrderId = shipmentOrderId;
    }

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public Long getLorryId() {
        return lorryId;
    }

    public void setLorryId(Long lorryId) {
        this.lorryId = lorryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ShipmentId)) return false;
        ShipmentId that = (ShipmentId) o;
        return Objects.equals(getShipmentOrderId(), that.getShipmentOrderId()) &&
                Objects.equals(getDriverId(), that.getDriverId()) &&
                Objects.equals(getLorryId(), that.getLorryId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getShipmentOrderId(), getDriverId(), getLorryId());
    }

    @Override
    public String toString() {
        return "ShipmentId{" +
                "shipmentOrderId=" + shipmentOrderId +
                ", driverId=" + driverId +
                ", lorryId=" + lorryId +
                '}';
    }
}
