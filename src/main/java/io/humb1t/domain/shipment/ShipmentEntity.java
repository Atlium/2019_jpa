package io.humb1t.domain.shipment;

import io.humb1t.domain.car.LorryEntity;
import io.humb1t.domain.employee.DriverEntity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@NamedQuery(name = "ShipmentEntity.findByShipmentDateAndDriverCount",
        query = "SELECT se " +
                "FROM ShipmentEntity se " +
                "JOIN se.shipmentOrder o " +
                "JOIN se.driver d " +
                "WHERE o.shipmentDate = :shipmentDate " +
                "GROUP BY se, o " +
                "HAVING COUNT(d)>:driverCount " +
                "ORDER BY o.shipmentDate DESC, o.address"
)
@Table(name = "shipments")
public class ShipmentEntity {

    @EmbeddedId
    private ShipmentId shipmentId = new ShipmentId();

    @ManyToOne(cascade = CascadeType.PERSIST)
    @MapsId("shipmentOrderId")
    @JoinColumn(name = "shipment_order_id")
    private ShipmentOrderEntity shipmentOrder;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @MapsId("driverId")
    @JoinColumn(name = "driver_id")
    private DriverEntity driver;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @MapsId("lorryId")
    @JoinColumn(name = "lorry_id")
    private LorryEntity lorry;

    public ShipmentEntity() {
    }

    public ShipmentEntity(ShipmentId shipmentId, ShipmentOrderEntity shipmentOrder, DriverEntity driver, LorryEntity lorry) {
        this.shipmentId = shipmentId;
        this.shipmentOrder = shipmentOrder;
        this.driver = driver;
        this.lorry = lorry;
    }

    public ShipmentId getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(ShipmentId shipmentId) {
        this.shipmentId = shipmentId;
    }

    public ShipmentOrderEntity getShipmentOrder() {
        return shipmentOrder;
    }

    public void setShipmentOrder(ShipmentOrderEntity shipmentOrder) {
        this.shipmentOrder = shipmentOrder;
    }

    public DriverEntity getDriver() {
        return driver;
    }

    public void setDriver(DriverEntity driver) {
        this.driver = driver;
    }

    public LorryEntity getLorry() {
        return lorry;
    }

    public void setLorry(LorryEntity lorry) {
        this.lorry = lorry;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ShipmentEntity)) return false;
        ShipmentEntity that = (ShipmentEntity) o;
        return Objects.equals(getShipmentId(), that.getShipmentId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getShipmentId());
    }

    @Override
    public String toString() {
        return "ShipmentEntity{" +
                "shipmentId=" + shipmentId +
                '}';
    }
}
