package io.humb1t.domain.car;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "lorries")
public class LorryEntity extends CarEntity {

    @Column
    private Integer carrying;

    public LorryEntity() {
    }

    public LorryEntity(Long id) {
        super(id);
    }

    public LorryEntity(String model, Integer maxSpeed, String producer, Integer carrying) {
        super(model, maxSpeed, producer);
        this.carrying = carrying;
    }

    public LorryEntity(Long id, String model, Integer maxSpeed, String producer, Integer carrying) {
        super(id, model, maxSpeed, producer);
        this.carrying = carrying;
    }

    public Integer getCarrying() {
        return carrying;
    }

    public void setCarrying(Integer carrying) {
        this.carrying = carrying;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LorryEntity)) return false;
        if (!super.equals(o)) return false;
        LorryEntity that = (LorryEntity) o;
        return Objects.equals(getCarrying(), that.getCarrying());
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), getCarrying());
    }

    @Override
    public String toString() {
        return "LorryEntity{" +
                "carrying=" + carrying +
                "} " + super.toString();
    }
}
