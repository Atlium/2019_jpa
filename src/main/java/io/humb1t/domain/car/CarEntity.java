package io.humb1t.domain.car;

import javax.persistence.*;
import java.util.Objects;

@Entity
@SequenceGenerator(name = "jpaSequence", sequenceName = "jpa_sequence", allocationSize = 1)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Table(name = "cars")
public class CarEntity extends CarTemplate {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "jpaSequence")
    private Long id;

    public CarEntity() {
    }

    public CarEntity(Long id) {
        this.id = id;
    }

    public CarEntity(String model, Integer maxSpeed, String producer) {
        super(model, maxSpeed, producer);
    }

    public CarEntity(Long id, String model, Integer maxSpeed, String producer) {
        super(model, maxSpeed, producer);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CarEntity)) return false;
        CarEntity carEntity = (CarEntity) o;
        return Objects.equals(getId(), carEntity.getId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "CarEntity{" +
                "id=" + id +
                "} " + super.toString();
    }
}
