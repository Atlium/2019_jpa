package io.humb1t.domain.car;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class CarTemplate {

    @Column
    private String model;

    @Column(name = "max_speed")
    private Integer maxSpeed;

    @Column(name = "producer")
    private String producer;

    public CarTemplate() {
    }

    public CarTemplate(String model, Integer maxSpeed, String producer) {
        this.model = model;
        this.maxSpeed = maxSpeed;
        this.producer = producer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(Integer maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    @Override
    public String toString() {
        return "CarTemplate{" +
                "model='" + model + '\'' +
                ", maxSpeed=" + maxSpeed +
                ", producer='" + producer + '\'' +
                '}';
    }
}
