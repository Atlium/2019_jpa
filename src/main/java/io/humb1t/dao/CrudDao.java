package io.humb1t.dao;

import java.util.List;

public interface CrudDao<E, K> {

    E create(E entity);

    boolean remove(K id);

    E update(E entity);

    List<E> findAll();

    E find(K id);
}
