package io.humb1t.dao;

import io.humb1t.domain.car.LorryEntity;

public interface LorryDao extends CrudDao<LorryEntity, Long> {
}
