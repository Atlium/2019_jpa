package io.humb1t.dao;

import io.humb1t.domain.employee.DriverEntity;

public interface DriverDao extends CrudDao<DriverEntity, Long> {
    DriverEntity findOldestByDepartmentId(Long id);
}
