package io.humb1t.dao;

import io.humb1t.domain.shipment.ShipmentEntity;
import io.humb1t.domain.shipment.ShipmentId;

import java.util.Date;
import java.util.List;

public interface ShipmentDao extends CrudDao<ShipmentEntity, ShipmentId> {
}
