package io.humb1t.dao;

import io.humb1t.domain.employee.DepartmentEntity;

import java.util.Date;
import java.util.List;

public interface DepartmentDao extends CrudDao<DepartmentEntity, Long> {

    List<DepartmentEntity> findMostPopular();

    List<DepartmentEntity> findByManBirthdaysNative(Date from, Date to);
    List<DepartmentEntity> findByManBirthdaysJpql(Date from, Date to);
    List<DepartmentEntity> findByManBirthdaysNamed(Date from, Date to);
    List<DepartmentEntity> findByManBirthdaysCriteria(Date from, Date to);
}
