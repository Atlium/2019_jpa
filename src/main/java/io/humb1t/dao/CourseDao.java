package io.humb1t.dao;

import io.humb1t.domain.activity.CourseEntity;

public interface CourseDao extends CrudDao<CourseEntity, Long> {
}
