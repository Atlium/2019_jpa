package io.humb1t.dao;

import io.humb1t.domain.car.CarEntity;

import java.util.List;

public interface CarDao extends CrudDao<CarEntity, Long> {

    List<CarEntity> findMostPopularMarks();
}
