package io.humb1t.dao;

import io.humb1t.domain.employee.ManEntity;

import java.util.List;

public interface ManDao extends CrudDao<ManEntity, Long> {

    List<ManEntity> findAllUnemployed();
}
