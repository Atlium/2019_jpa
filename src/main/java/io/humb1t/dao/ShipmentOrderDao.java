package io.humb1t.dao;

import io.humb1t.domain.shipment.ShipmentOrderEntity;

import java.util.List;

public interface ShipmentOrderDao extends CrudDao<ShipmentOrderEntity, Long> {
    List<ShipmentOrderEntity> findOrdersWithShipmentsCountNative(Long shipmentCount);
    List<ShipmentOrderEntity> findOrdersWithShipmentsCountJpql(Long shipmentCount);
    List<ShipmentOrderEntity> findOrdersWithShipmentsCountNamed(Long shipmentCount);
    List<ShipmentOrderEntity> findOrdersWithShipmentsCountCriteria(Long shipmentCount);
}
