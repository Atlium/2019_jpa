package io.humb1t.dao.jpa;

import io.humb1t.dao.ShipmentDao;
import io.humb1t.domain.car.LorryEntity;
import io.humb1t.domain.employee.DriverEntity;
import io.humb1t.domain.shipment.ShipmentEntity;
import io.humb1t.domain.shipment.ShipmentId;
import io.humb1t.domain.shipment.ShipmentOrderEntity;
import io.humb1t.exception.ExecutionException;
import io.humb1t.utils.db.JpaUtils;
import io.humb1t.utils.db.RequestWrapper;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;

public class ShipmentDaoJpa extends CrudDaoJpa<ShipmentEntity, ShipmentId> implements ShipmentDao {

    private static final Logger LOGGER = Logger.getLogger(EmployeeDaoJpa.class);
    private static final String NULL_VALUE_MESSAGE = "Null value";
    private static final String SINGLE_PARAMETER_FORMAT = "params:%s";

    @Override
    public ShipmentEntity create(ShipmentEntity entity) {
        LOGGER.info(String.format(SINGLE_PARAMETER_FORMAT, entity));
        if (isNotValidEntityIgnoreId(entity)) {
            throw new IllegalArgumentException(NULL_VALUE_MESSAGE);
        }
        LOGGER.info(String.format("Start creating entity: %s", entity));
        try {
            return RequestWrapper.wrapEntityManagerRequest(getEntityManager(), em -> {
                DriverEntity driver = entity.getDriver();
                LorryEntity lorry = entity.getLorry();
                ShipmentOrderEntity shipmentOrder = entity.getShipmentOrder();
                entity.setDriver(driver.getId() == null ? driver : em.find(driver.getClass(), driver.getId()));
                entity.setLorry(lorry.getId() == null ? lorry : em.find(lorry.getClass(), lorry.getId()));
                entity.setShipmentOrder(shipmentOrder.getId() == null ? shipmentOrder : em.find(shipmentOrder.getClass(), shipmentOrder.getId()));
                em.persist(entity);
                return entity;
            });
        } catch (Exception e) {
            throw new ExecutionException(String.format("Can't persist entity:%s", entity));
        }
    }

    @Override
    protected Class<ShipmentEntity> getEntityClass() {
        return ShipmentEntity.class;
    }

    @Override
    protected EntityManager getEntityManager() {
        return JpaUtils.getEntityManager();
    }

    @Override
    protected boolean isNotValidEntity(ShipmentEntity entity) {
        return entity == null
                || entity.getShipmentId() == null
                || entity.getShipmentId().getDriverId() == null
                || entity.getShipmentId().getLorryId() == null
                || entity.getShipmentId().getShipmentOrderId() == null
                || entity.getDriver() == null
                || entity.getDriver().getId() == null
                || entity.getLorry() == null
                || entity.getLorry().getId() == null
                || entity.getShipmentOrder() == null
                || entity.getShipmentOrder().getId() == null;
    }

    @Override
    protected boolean isNotValidEntityIgnoreId(ShipmentEntity entity) {
        return entity == null
                || entity.getShipmentId() == null
                || entity.getDriver() == null
                || entity.getDriver().getId() == null
                || entity.getLorry() == null
                || entity.getLorry().getId() == null
                || entity.getShipmentOrder() == null
                || entity.getShipmentOrder().getId() == null;
    }
}
