package io.humb1t.dao.jpa;

import io.humb1t.dao.ShipmentOrderDao;
import io.humb1t.domain.shipment.ShipmentEntity;
import io.humb1t.domain.shipment.ShipmentOrderEntity;
import io.humb1t.utils.db.JpaUtils;
import io.humb1t.utils.db.RequestWrapper;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.List;

public class ShipmentOrderDaoJpa extends CrudDaoJpa<ShipmentOrderEntity, Long> implements ShipmentOrderDao {

    @Override
    protected Class<ShipmentOrderEntity> getEntityClass() {
        return ShipmentOrderEntity.class;
    }

    @Override
    protected EntityManager getEntityManager() {
        return JpaUtils.getEntityManager();
    }

    @Override
    protected boolean isNotValidEntity(ShipmentOrderEntity entity) {
        return entity == null
                || entity.getId() == null
                || entity.getAddress() == null
                || entity.getShipmentDate() == null;
    }

    @Override
    protected boolean isNotValidEntityIgnoreId(ShipmentOrderEntity entity) {
        return entity == null
                || entity.getAddress() == null
                || entity.getShipmentDate() == null;
    }

    @Override
    public List<ShipmentOrderEntity> findOrdersWithShipmentsCountNative(Long shipmentCount) {
        final String SQL = "SELECT o.id, o.address, o.shipment_date " +
                "FROM shipment_orders o " +
                "JOIN shipments s ON s.shipment_order_id = o.id " +
                "GROUP BY o.id, o.address, o.shipment_date " +
                "HAVING COUNT(s) = :shipmentCount " +
                "ORDER BY o.address";
        return RequestWrapper.wrapEntityManagerRequest(getEntityManager(), em ->
                em.createNativeQuery(SQL, ShipmentOrderEntity.class)
                        .setParameter("shipmentCount", shipmentCount)
                        .getResultList());
    }

    @Override
    public List<ShipmentOrderEntity> findOrdersWithShipmentsCountJpql(Long shipmentCount) {
        final String JPQL = "SELECT o " +
                "FROM ShipmentEntity s " +
                "JOIN s.shipmentOrder o " +
                "GROUP BY o " +
                "HAVING COUNT(s) = :shipmentCount " +
                "ORDER BY o.address";
        return RequestWrapper.wrapEntityManagerRequest(getEntityManager(), em ->
                em.createQuery(JPQL, ShipmentOrderEntity.class)
                        .setParameter("shipmentCount", shipmentCount)
                        .getResultList());
    }

    @Override
    public List<ShipmentOrderEntity> findOrdersWithShipmentsCountNamed(Long shipmentCount) {
        return RequestWrapper.wrapEntityManagerRequest(getEntityManager(), em ->
                em.createNamedQuery("ShipmentOrderEntity.findOrdersWithShipmentsCount", ShipmentOrderEntity.class)
                        .setParameter("shipmentCount", shipmentCount)
                        .getResultList());
    }

    @Override
    public List<ShipmentOrderEntity> findOrdersWithShipmentsCountCriteria(Long shipmentCount) {
        return RequestWrapper.wrapEntityManagerRequest(getEntityManager(), em -> {
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
            CriteriaQuery<ShipmentOrderEntity> query = criteriaBuilder.createQuery(ShipmentOrderEntity.class);
            Root<ShipmentEntity> shipment = query.from(ShipmentEntity.class);
            Join<ShipmentEntity, ShipmentOrderEntity> order = shipment.join("shipmentOrder");
            query
                    .select(shipment.get("shipmentOrder"))
                    .groupBy(order)
                    .having(criteriaBuilder.equal(criteriaBuilder.count(shipment), shipmentCount))
                    .orderBy(criteriaBuilder.asc(order.get("address")));
            return em.createQuery(query).getResultList();
        });
    }
}
