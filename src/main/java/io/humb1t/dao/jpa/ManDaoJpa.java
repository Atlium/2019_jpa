package io.humb1t.dao.jpa;

import io.humb1t.dao.ManDao;
import io.humb1t.domain.employee.ManEntity;
import io.humb1t.utils.db.JpaUtils;
import io.humb1t.utils.db.RequestWrapper;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public class ManDaoJpa extends CrudDaoJpa<ManEntity, Long> implements ManDao {

    @Override
    protected Class<ManEntity> getEntityClass() {
        return ManEntity.class;
    }

    @Override
    protected EntityManager getEntityManager() {
        return JpaUtils.getEntityManager();
    }

    @Override
    protected boolean isNotValidEntity(ManEntity man) {
        return man == null
                || man.getId() == null
                || man.getBirthDate() == null
                || man.getFirstName() == null
                || man.getLastName() == null;
    }

    @Override
    protected boolean isNotValidEntityIgnoreId(ManEntity man) {
        return man == null
                || man.getBirthDate() == null
                || man.getFirstName() == null
                || man.getLastName() == null;
    }

    @Override
    public List<ManEntity> findAllUnemployed() {
        return RequestWrapper.wrapEntityManagerRequest(getEntityManager(),
                em -> em.createNamedQuery("ManEntity.findAllUnemployed", getEntityClass()).getResultList());
    }
}
