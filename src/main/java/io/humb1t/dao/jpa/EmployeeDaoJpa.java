package io.humb1t.dao.jpa;

import io.humb1t.dao.EmployeeDao;
import io.humb1t.domain.employee.DepartmentEntity;
import io.humb1t.domain.employee.EmployeeEntity;
import io.humb1t.domain.employee.ManEntity;
import io.humb1t.exception.ExecutionException;
import io.humb1t.utils.date.DateUtils;
import io.humb1t.utils.db.JpaUtils;
import io.humb1t.utils.db.RequestWrapper;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EmployeeDaoJpa extends CrudDaoJpa<EmployeeEntity, Long> implements EmployeeDao {

    private static final Logger LOGGER = Logger.getLogger(EmployeeDaoJpa.class);
    private static final String NULL_VALUE_MESSAGE = "Null value";
    private static final String SINGLE_PARAMETER_FORMAT = "params:%s";

    @Override
    public List<EmployeeEntity> findAllByDepartmentIdAndWorkExperience(Long id, int ages) {
        LOGGER.info(String.format("params:%s, %s", id, ages));
        LOGGER.info("Start find all by department id and work experience");
        return RequestWrapper.wrapEntityManagerRequest(getEntityManager(), em -> {
            Date currentDate = new Date();
            Date startDate = DateUtils.getWithAddedYears(currentDate, -ages);
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<EmployeeEntity> query = builder.createQuery(EmployeeEntity.class);
            Root<EmployeeEntity> employee = query.from(EmployeeEntity.class);
            Join<EmployeeEntity, DepartmentEntity> department = employee.join("department");
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.lessThan(employee.get("employmentDate"), startDate));
            predicates.add(builder.equal(department.get("id"), id));
            query
                    .select(employee)
                    .where(predicates.toArray(new Predicate[]{}))
                    .orderBy(builder.asc(employee.get("man").get("lastName")), builder.asc(employee.get("man").get("firstName")));
            return em.createQuery(query).getResultList();
        });
    }

    @Override
    public EmployeeEntity create(EmployeeEntity entity) {
        LOGGER.info(String.format(SINGLE_PARAMETER_FORMAT, entity));
        if (isNotValidEntityIgnoreId(entity)) {
            throw new IllegalArgumentException(NULL_VALUE_MESSAGE);
        }
        LOGGER.info(String.format("Start creating entity: %s", entity));
        try {
            return RequestWrapper.wrapEntityManagerRequest(getEntityManager(), em -> {
                ManEntity man = entity.getMan();
                DepartmentEntity department = entity.getDepartment();
                entity.setMan(man.getId() == null ? man : em.find(man.getClass(), man.getId()));
                entity.setDepartment(department.getId() == null ? department : em.find(department.getClass(), department.getId()));
                em.persist(entity);
                return entity;
            });
        } catch (Exception e) {
            throw new ExecutionException(String.format("Can't persist entity:%s", entity));
        }
    }

    @Override
    protected Class<EmployeeEntity> getEntityClass() {
        return EmployeeEntity.class;
    }

    @Override
    protected EntityManager getEntityManager() {
        return JpaUtils.getEntityManager();
    }

    @Override
    protected boolean isNotValidEntity(EmployeeEntity employeeEntity) {
        return employeeEntity == null
                || employeeEntity.getId() == null
                || employeeEntity.getMan() == null
                || employeeEntity.getMan().getId() == null
                || employeeEntity.getDepartment() == null
                || employeeEntity.getDepartment().getId() == null
                || employeeEntity.getEmploymentDate() == null;
    }

    @Override
    protected boolean isNotValidEntityIgnoreId(EmployeeEntity employeeEntity) {
        return employeeEntity == null
                || employeeEntity.getMan() == null
                || employeeEntity.getDepartment() == null
                || employeeEntity.getEmploymentDate() == null;
    }
}
