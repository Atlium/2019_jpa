package io.humb1t.dao.jpa;

import io.humb1t.dao.CarDao;
import io.humb1t.domain.car.CarEntity;
import io.humb1t.utils.db.JpaUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class CarDaoJpa extends CrudDaoJpa<CarEntity, Long> implements CarDao {

    @Override
    protected Class<CarEntity> getEntityClass() {
        return CarEntity.class;
    }

    @Override
    protected EntityManager getEntityManager() {
        return JpaUtils.getEntityManager();
    }

    @Override
    protected boolean isNotValidEntity(CarEntity entity) {
        return entity == null
                || entity.getId() == null
                || entity.getMaxSpeed() == null
                || entity.getModel() == null
                || entity.getProducer() == null;
    }

    @Override
    protected boolean isNotValidEntityIgnoreId(CarEntity entity) {
        return entity == null
                || entity.getMaxSpeed() == null
                || entity.getModel() == null
                || entity.getProducer() == null;
    }

    @Override
    public List<CarEntity> findMostPopularMarks() {
        return null;
    }
}
