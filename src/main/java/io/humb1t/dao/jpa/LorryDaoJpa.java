package io.humb1t.dao.jpa;

import io.humb1t.dao.LorryDao;
import io.humb1t.domain.car.LorryEntity;
import io.humb1t.utils.db.JpaUtils;

import javax.persistence.EntityManager;

public class LorryDaoJpa extends CrudDaoJpa<LorryEntity, Long> implements LorryDao {
    @Override
    protected Class<LorryEntity> getEntityClass() {
        return LorryEntity.class;
    }

    @Override
    protected EntityManager getEntityManager() {
        return JpaUtils.getEntityManager();
    }

    @Override
    protected boolean isNotValidEntity(LorryEntity entity) {
        return entity == null
                || entity.getId() == null
                || entity.getCarrying() == null
                || entity.getMaxSpeed() == null
                || entity.getModel() == null
                || entity.getProducer() == null;
    }

    @Override
    protected boolean isNotValidEntityIgnoreId(LorryEntity entity) {
        return entity == null
                || entity.getCarrying() == null
                || entity.getMaxSpeed() == null
                || entity.getModel() == null
                || entity.getProducer() == null;
    }
}
