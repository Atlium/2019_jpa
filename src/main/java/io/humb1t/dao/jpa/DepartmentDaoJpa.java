package io.humb1t.dao.jpa;

import io.humb1t.dao.DepartmentDao;
import io.humb1t.domain.employee.DepartmentEntity;
import io.humb1t.domain.employee.EmployeeEntity;
import io.humb1t.domain.employee.ManEntity;
import io.humb1t.utils.db.JpaUtils;
import io.humb1t.utils.db.RequestWrapper;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class DepartmentDaoJpa extends CrudDaoJpa<DepartmentEntity, Long> implements DepartmentDao {

    @Override
    protected Class<DepartmentEntity> getEntityClass() {
        return DepartmentEntity.class;
    }

    @Override
    protected EntityManager getEntityManager() {
        return JpaUtils.getEntityManager();
    }

    @Override
    protected boolean isNotValidEntity(DepartmentEntity department) {
        return department == null
                || department.getId() == null
                || department.getDeptName() == null
                || department.getFoundationDate() == null;
    }

    @Override
    protected boolean isNotValidEntityIgnoreId(DepartmentEntity department) {
        return department == null
                || department.getDeptName() == null
                || department.getFoundationDate() == null;
    }

    @Override
    public List<DepartmentEntity> findMostPopular() {
        final String SQL = "SELECT d.id, d.dept_name, d.foundation_date " +
                "FROM departments d " +
                "JOIN employees e ON e.department_id = d.id " +
                "GROUP BY d.id " +
                "HAVING COUNT(e.id) = (" +
                "   SELECT COUNT(e.id) total_count " +
                "   FROM departments d " +
                "   JOIN employees e ON e.department_id = d.id " +
                "   GROUP BY d.id " +
                "   ORDER BY total_count DESC " +
                "   LIMIT 1" +
                ")";
        return RequestWrapper.wrapEntityManagerRequest(getEntityManager(), em ->
                em.createNativeQuery(SQL, DepartmentEntity.class).getResultList());
    }

    @Override
    public List<DepartmentEntity> findByManBirthdaysNative(Date from, Date to) {
        final String SQL = "SELECT d.id, d.dept_name, d.foundation_date " +
                "FROM departments d " +
                "JOIN employees e ON e.department_id = d.id " +
                "JOIN mans m ON m.id = e.man_id " +
                "WHERE m.birth_date BETWEEN :fromDate AND :toDate " +
                "GROUP BY d.id, d.dept_name, d.foundation_date " +
                "HAVING COUNT(e.id) >= 3 " +
                "ORDER BY d.dept_name";
        return RequestWrapper.wrapEntityManagerRequest(getEntityManager(), em ->
                em.createNativeQuery(SQL, DepartmentEntity.class)
                        .setParameter("fromDate", from)
                        .setParameter("toDate", to)
                        .getResultList());
    }

    @Override
    public List<DepartmentEntity> findByManBirthdaysJpql(Date from, Date to) {
        final String JPQL = "SELECT d " +
                "FROM EmployeeEntity e " +
                "JOIN e.department d " +
                "JOIN e.man m " +
                "WHERE m.birthDate BETWEEN :fromDate AND :toDate " +
                "GROUP BY d " +
                "HAVING COUNT(e) >= 3 " +
                "ORDER BY d.deptName";
        return RequestWrapper.wrapEntityManagerRequest(getEntityManager(), em ->
                em.createQuery(JPQL, DepartmentEntity.class)
                        .setParameter("fromDate", from)
                        .setParameter("toDate", to)
                        .getResultList());
    }

    @Override
    public List<DepartmentEntity> findByManBirthdaysNamed(Date from, Date to) {
        return RequestWrapper.wrapEntityManagerRequest(getEntityManager(), em ->
                em.createNamedQuery("DepartmentEntity.findByManBirthdays", DepartmentEntity.class)
                        .setParameter("fromDate", from)
                        .setParameter("toDate", to)
                        .getResultList());
    }

    @Override
    public List<DepartmentEntity> findByManBirthdaysCriteria(Date from, Date to) {
        return RequestWrapper.wrapEntityManagerRequest(getEntityManager(), em -> {
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
            CriteriaQuery<DepartmentEntity> query = criteriaBuilder.createQuery(DepartmentEntity.class);
            Root<EmployeeEntity> employee = query.from(EmployeeEntity.class);
            Join<EmployeeEntity, DepartmentEntity> department = employee.join("department");
            Join<EmployeeEntity, ManEntity> man = employee.join("man");
            query
                    .select(employee.get("department"))
                    .where(criteriaBuilder.between(man.get("birthDate"), from, to))
                    .groupBy(department)
                    .having(criteriaBuilder.greaterThanOrEqualTo(criteriaBuilder.count(employee.get("id")), 3L))
                    .orderBy(criteriaBuilder.asc(department.get("deptName")));
            return em.createQuery(query).getResultList();
        });
    }
}
