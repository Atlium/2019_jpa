package io.humb1t.dao.jpa;

import io.humb1t.dao.DriverDao;
import io.humb1t.domain.employee.DepartmentEntity;
import io.humb1t.domain.employee.DriverEntity;
import io.humb1t.domain.employee.ManEntity;
import io.humb1t.exception.ExecutionException;
import io.humb1t.utils.db.JpaUtils;
import io.humb1t.utils.db.RequestWrapper;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;

public class DriverDaoJpa extends CrudDaoJpa<DriverEntity, Long> implements DriverDao {

    private static final Logger LOGGER = Logger.getLogger(DriverDaoJpa.class);
    private static final String NULL_VALUE_MESSAGE = "Null value";
    private static final String SINGLE_PARAMETER_FORMAT = "params:%s";

    @Override
    public DriverEntity create(DriverEntity entity) {
        LOGGER.info(String.format(SINGLE_PARAMETER_FORMAT, entity));
        if (isNotValidEntityIgnoreId(entity)) {
            throw new IllegalArgumentException(NULL_VALUE_MESSAGE);
        }
        LOGGER.info(String.format("Start creating entity: %s", entity));
        try {
            return RequestWrapper.wrapEntityManagerRequest(getEntityManager(), em -> {
                ManEntity man = entity.getMan();
                DepartmentEntity department = entity.getDepartment();
                entity.setMan(man.getId() == null ? man : em.find(man.getClass(), man.getId()));
                entity.setDepartment(department.getId() == null ? department : em.find(department.getClass(), department.getId()));
                em.persist(entity);
                return entity;
            });
        } catch (Exception e) {
            throw new ExecutionException(String.format("Can't persist entity:%s", entity));
        }
    }

    @Override
    public DriverEntity findOldestByDepartmentId(Long id) {
        LOGGER.info("Start finding oldest employee");
        final String JPQL = "SELECT dr " +
                "FROM DriverEntity dr " +
                "JOIN dr.department d " +
                "JOIN dr.man m " +
                "WHERE d.id = :deptId " +
                "AND m.birthDate = (" +
                "SELECT MIN(m.birthDate) " +
                "FROM ManEntity m " +
                ")";
        return RequestWrapper.wrapEntityManagerRequest(getEntityManager(), em ->
                em.createQuery(JPQL, DriverEntity.class).setParameter("deptId", id).setMaxResults(1).getSingleResult());
    }

    @Override
    protected Class<DriverEntity> getEntityClass() {
        return DriverEntity.class;
    }

    @Override
    protected EntityManager getEntityManager() {
        return JpaUtils.getEntityManager();
    }

    @Override
    protected boolean isNotValidEntity(DriverEntity driverEntity) {
        return driverEntity == null
                || driverEntity.getId() == null
                || driverEntity.getMan() == null
                || driverEntity.getMan().getId() == null
                || driverEntity.getDepartment() == null
                || driverEntity.getDepartment().getId() == null
                || driverEntity.getEmploymentDate() == null;
    }

    @Override
    protected boolean isNotValidEntityIgnoreId(DriverEntity driverEntity) {
        return driverEntity == null
                || driverEntity.getMan() == null
                || driverEntity.getDepartment() == null
                || driverEntity.getEmploymentDate() == null;
    }
}
