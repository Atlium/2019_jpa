package io.humb1t.dao.jpa;

import io.humb1t.dao.CrudDao;
import io.humb1t.exception.ExecutionException;
import io.humb1t.exception.NotFoundException;
import io.humb1t.utils.db.RequestWrapper;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public abstract class CrudDaoJpa<E, K> implements CrudDao<E, K> {

    private static final Logger LOGGER = Logger.getLogger(CrudDaoJpa.class);
    private static final String NULL_VALUE_MESSAGE = "Null value";
    private static final String SINGLE_PARAMETER_FORMAT = "params:%s";

    @Override
    public E create(E entity) {
        LOGGER.info(String.format(SINGLE_PARAMETER_FORMAT, entity));
        if (isNotValidEntityIgnoreId(entity)) {
            throw new IllegalArgumentException(NULL_VALUE_MESSAGE);
        }
        LOGGER.info(String.format("Start creating entity: %s", entity));
        try {
            return RequestWrapper.wrapEntityManagerRequest(getEntityManager(), em -> {
                em.persist(entity);
                return entity;
            });
        } catch (Exception e) {
            throw new ExecutionException(String.format("Can't persist entity:%s", entity));
        }
    }

    @Override
    public boolean remove(K id) {
        LOGGER.info(String.format(SINGLE_PARAMETER_FORMAT, id));
        if (id == null) {
            throw new IllegalArgumentException(NULL_VALUE_MESSAGE);
        }
        LOGGER.info(String.format("Start removing entity with id: %s", id));
        try {
            return RequestWrapper.wrapEntityManagerRequest(getEntityManager(), em -> {
                em.remove(em.find(getEntityClass(), id));
                return true;
            });
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public E update(E entity) {
        LOGGER.info(String.format(SINGLE_PARAMETER_FORMAT, entity));
        if (isNotValidEntity(entity)) {
            throw new IllegalArgumentException(NULL_VALUE_MESSAGE);
        }
        LOGGER.info(String.format("Start updating entity: %s", entity));
        try {
            return RequestWrapper.wrapEntityManagerRequest(getEntityManager(), em -> em.merge(entity));
        } catch (Exception e) {
            throw new ExecutionException(String.format("Can't update entity:%s", entity));
        }
    }

    @Override
    public List<E> findAll() {
        LOGGER.info("Start finding all entities");
        return RequestWrapper.wrapEntityManagerRequest(getEntityManager(), em -> {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<E> query = builder.createQuery(getEntityClass());
            Root<E> variableRoot = query.from(getEntityClass());
            query.select(variableRoot);
            return em.createQuery(query).getResultList();
        });
    }

    @Override
    public E find(K id) {
        LOGGER.info(String.format(SINGLE_PARAMETER_FORMAT, id));
        if (id == null) {
            throw new IllegalArgumentException(NULL_VALUE_MESSAGE);
        }
        return RequestWrapper.wrapEntityManagerRequest(getEntityManager(), em -> {
            E entity = em.find(getEntityClass(), id);
            if (entity == null) {
                throw new NotFoundException(String.format("Entity with id %s not found", id));
            }
            return entity;
        });
    }

    protected abstract Class<E> getEntityClass();

    protected abstract EntityManager getEntityManager();

    protected abstract boolean isNotValidEntity(E entity);

    protected abstract boolean isNotValidEntityIgnoreId(E entity);
}
