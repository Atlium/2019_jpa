package io.humb1t.dao.jpa;

import io.humb1t.dao.CourseDao;
import io.humb1t.domain.activity.CourseEntity;
import io.humb1t.utils.db.JpaUtils;

import javax.persistence.EntityManager;

public class CourseDaoJpa extends CrudDaoJpa<CourseEntity, Long> implements CourseDao {

    @Override
    protected Class<CourseEntity> getEntityClass() {
        return CourseEntity.class;
    }

    @Override
    protected EntityManager getEntityManager() {
        return JpaUtils.getEntityManager();
    }

    @Override
    protected boolean isNotValidEntity(CourseEntity entity) {
        return entity == null
                || entity.getId() == null
                || entity.getName() == null
                || entity.getDuration() == null;
    }

    @Override
    protected boolean isNotValidEntityIgnoreId(CourseEntity entity) {
        return entity == null
                || entity.getName() == null
                || entity.getDuration() == null;
    }
}
