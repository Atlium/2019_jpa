package io.humb1t.dao;

import io.humb1t.domain.employee.EmployeeEntity;

import java.util.List;

public interface EmployeeDao extends CrudDao<EmployeeEntity, Long> {

    List<EmployeeEntity> findAllByDepartmentIdAndWorkExperience(Long id, int ages);
}
