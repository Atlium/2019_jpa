package io.humb1t;

import io.humb1t.dao.DepartmentDao;
import io.humb1t.dao.jpa.DepartmentDaoJpa;
import io.humb1t.domain.employee.DepartmentEntity;
import io.humb1t.utils.db.JpaUtils;
import org.apache.log4j.Logger;

import java.util.Date;

public class Main {
    private static final Logger LOGGER = Logger.getLogger(Main.class);
    private static final String DELETE_FUNC_SCRIPT_FILE = "drop_func_and_trigger.sql";
    private static final String DROP_SCRIPT_FILE = "drop.sql";


    public static void main(String[] args) {
        DepartmentDao departmentDao = new DepartmentDaoJpa();
        departmentDao.create(new DepartmentEntity(null, "QweDept", new Date()));
        JpaUtils.closeFactory();
        //        removeAllTablesAndFunctions();
    }
}
