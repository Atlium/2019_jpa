package io.humb1t.exception;

public class NotFoundException extends ExecutionException {

    private static final long serialVersionUID = 6524950149027282219L;

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
