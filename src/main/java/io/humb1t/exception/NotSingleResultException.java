package io.humb1t.exception;

public class NotSingleResultException extends ExecutionException {

    private static final long serialVersionUID = -6585056213707737945L;

    public NotSingleResultException(String message) {
        super(message);
    }

    public NotSingleResultException(String message, Throwable cause) {
        super(message, cause);
    }
}
