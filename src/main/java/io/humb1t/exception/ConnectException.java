package io.humb1t.exception;

public class ConnectException extends RuntimeException {
    private static final long serialVersionUID = 7000981163154497096L;

    public ConnectException(Throwable cause) {
        super(cause);
    }
}
