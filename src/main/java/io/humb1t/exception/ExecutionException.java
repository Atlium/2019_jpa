package io.humb1t.exception;

public class ExecutionException extends RuntimeException {

    private static final long serialVersionUID = -2786016597425009463L;

    public ExecutionException(Throwable cause) {
        super(cause);
    }

    public ExecutionException(String message) {
        super(message);
    }

    public ExecutionException(String message, Throwable cause) {
        super(message, cause);
    }
}
