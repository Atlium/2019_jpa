package io.humb1t.utils.date;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateUtils {
    private static final String TIMESTAMP_FORMAT = "dd.MM.yyyy HH:mm:ss";

    private DateUtils() {
        //util
    }

    public static Date getSqlDate(java.util.Date date) {
        if (date == null) {
            return null;
        }
        return new Date(date.getTime());
    }

    public static java.util.Date getUtilDate(Date date) {
        if (date == null) {
            return null;
        }
        return new Date(date.getTime());
    }

    public static String formatToTimestamp(java.util.Date date) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat(TIMESTAMP_FORMAT).format(date);
    }

    public static java.util.Date getWithAddedYears(java.util.Date date, int yearCount) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, yearCount);
        return calendar.getTime();
    }
}
