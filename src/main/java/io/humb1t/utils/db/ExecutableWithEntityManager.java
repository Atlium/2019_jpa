package io.humb1t.utils.db;

import javax.persistence.EntityManager;

public interface ExecutableWithEntityManager<T> {
    T executeRequest(EntityManager entityManager) throws Exception;
}
