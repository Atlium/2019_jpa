package io.humb1t.utils.db;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaUtils {
    private static EntityManagerFactory entityManagerFactory;

    static {
        iniyFactory();
    }

    private static void iniyFactory() {
        entityManagerFactory = Persistence.createEntityManagerFactory("CoursesUnit");
    }

    private JpaUtils() {
        //util
    }

    public static EntityManager getEntityManager() {
        if (!entityManagerFactory.isOpen()) {
            iniyFactory();
        }
        return entityManagerFactory.createEntityManager();
    }

    public static void closeFactory() {
        entityManagerFactory.close();
    }
}
