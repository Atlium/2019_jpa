package io.humb1t.utils.db;

import io.humb1t.exception.ConnectException;
import io.humb1t.utils.file.FileFactory;
import io.humb1t.utils.file.FileSource;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class ConnectionFactory {

    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class);
    private static final FileFactory FILE_FACTORY = new FileFactory();
    private static final String PROPERTIES_FILE = "db.properties";
    private static final FileSource PROPERTIES_SOURCE = FileSource.RESOURCE;
    private static String dbName;
    private static String url;
    private static String userName;
    private static String password;
    private static String driver;

    static {
        init();
    }

    private ConnectionFactory() {
        //util
    }

    public static Connection getConnection() {
        if (!isValid()) {
            init();
        }
        try {
            Class.forName(driver);
            return DriverManager.getConnection(url, userName, password);
        } catch (Exception e) {
            LOGGER.error("Connection no established", e);
            throw new ConnectException(e);
        }
    }

    private static void init() {
        try (FileInputStream input = new FileInputStream(FILE_FACTORY.getFile(PROPERTIES_SOURCE, PROPERTIES_FILE))) {
            Properties properties = new Properties();
            properties.load(input);
            dbName = properties.getProperty("jdbc.db.name");
            url = String.format(properties.getProperty("jdbc.url"), dbName);
            userName = properties.getProperty("jdbc.user.name");
            password = properties.getProperty("jdbc.user.password");
            driver = properties.getProperty("jdbc.driver");
        } catch (Exception e) {
            LOGGER.error("Error while reading property file", e);
        }
    }

    private static boolean isValid() {
        return dbName != null && !dbName.isEmpty()
                && url != null && !url.isEmpty()
                && userName != null && !userName.isEmpty()
                && password != null && !password.isEmpty()
                && driver != null && !driver.isEmpty();
    }
}
