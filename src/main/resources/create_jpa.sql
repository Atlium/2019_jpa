CREATE SEQUENCE jpa_sequence;

CREATE TABLE mans
(
  id         BIGINT       NOT NULL DEFAULT nextval('jpa_sequence') PRIMARY KEY,
  first_name VARCHAR(255) NOT NULL,
  last_name  VARCHAR(255) NOT NULL,
  birth_date TIMESTAMP    NOT NULL
);
CREATE TABLE departments
(
  id              BIGINT       NOT NULL DEFAULT nextval('jpa_sequence') PRIMARY KEY,
  dept_name       VARCHAR(255) NOT NULL,
  foundation_date TIMESTAMP    NOT NULL
);
CREATE TABLE employees
(
  id              BIGINT    NOT NULL DEFAULT nextval('jpa_sequence') PRIMARY KEY,
  man_id          BIGINT    NOT NULL,
  department_id   BIGINT    NOT NULL,
  employment_date TIMESTAMP NOT NULL,
  dismiss_date    TIMESTAMP,
  CONSTRAINT employees_mans_id_fk FOREIGN KEY (man_id) REFERENCES mans (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT employees_departments_id_fk FOREIGN KEY (department_id) REFERENCES departments (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT employees_check_dates CHECK (dismiss_date >= employment_date)
);
create table courses
(
  id       BIGINT NOT NULL DEFAULT nextval('jpa_sequence') PRIMARY KEY,
  duration INTEGER,
  name     VARCHAR(255)
);
create table employee_courses
(
  employee_id BIGINT NOT NULL,
  course_id   BIGINT NOT NULL,
  CONSTRAINT employee_courses_pk PRIMARY KEY (employee_id, course_id),
  CONSTRAINT employee_courses_employees_id_fk FOREIGN KEY (employee_id) REFERENCES employees (id),
  CONSTRAINT employee_courses_courses_id_fk FOREIGN KEY (course_id) REFERENCES courses (id)
);
CREATE TABLE drivers
(
  id BIGINT NOT NULL PRIMARY KEY,
  CONSTRAINT drivers_employees_id_fk FOREIGN KEY (id) REFERENCES employees (id)
);

CREATE TABLE driver_categories
(
  driver_id BIGINT NOT NULL,
  category  INTEGER,
  CONSTRAINT driver_categories_drivers_id_fk FOREIGN KEY (driver_id) REFERENCES drivers (id)
);

CREATE TABLE cars
(
  id        BIGINT NOT NULL DEFAULT nextval('jpa_sequence') PRIMARY KEY,
  max_speed INTEGER,
  model     VARCHAR(255),
  producer  VARCHAR(255)
);

CREATE TABLE lorries
(
  id        BIGINT NOT NULL DEFAULT nextval('jpa_sequence') PRIMARY KEY,
  max_speed INTEGER,
  model     VARCHAR(255),
  producer  VARCHAR(255),
  carrying  INTEGER
);

CREATE TABLE shipment_orders
(
  id            BIGINT NOT NULL DEFAULT nextval('jpa_sequence') PRIMARY KEY,
  address       VARCHAR(255),
  shipment_date TIMESTAMP
);

CREATE TABLE shipments
(
  driver_id         BIGINT NOT NULL,
  lorry_id          BIGINT NOT NULL,
  shipment_order_id BIGINT NOT NULL,
  CONSTRAINT shipments_pk PRIMARY KEY (driver_id, lorry_id, shipment_order_id),
  CONSTRAINT shipments_drivers_id_fk FOREIGN KEY (driver_id) REFERENCES drivers (id),
  CONSTRAINT shipments_lorries_id_fk FOREIGN KEY (lorry_id) REFERENCES lorries (id),
  CONSTRAINT shipments_shipment_orders_id_fk FOREIGN KEY (shipment_order_id) REFERENCES shipment_orders (id)
);

CREATE OR REPLACE FUNCTION check_employee_born_date()
  RETURNS TRIGGER AS
$cua$
BEGIN
  IF NEW.employment_date < (
    SELECT m.birth_date
    FROM mans m
    WHERE m.id = NEW.man_id
    LIMIT (1)
  )
  THEN
    RAISE EXCEPTION 'Man birth day cannot be later than hiring date';
  END IF;
  RETURN NEW;
END;
$cua$
LANGUAGE plpgsql;

CREATE TRIGGER on_create_or_update_employee_check_born_date_below_employment_date
  BEFORE INSERT OR UPDATE
  ON employees
  FOR EACH ROW
EXECUTE PROCEDURE check_employee_born_date();

CREATE OR REPLACE FUNCTION check_department_foundation_below_hiring_date()
  RETURNS TRIGGER AS
$cua$
BEGIN
  IF NEW.employment_date < (
    SELECT d.foundation_date
    FROM departments d
    WHERE d.id = NEW.department_id
    LIMIT (1)
  )
  THEN
    RAISE EXCEPTION 'Employment date cannot be earlier than department foundation date';
  END IF;
  RETURN NEW;
END;
$cua$
LANGUAGE plpgsql;

CREATE TRIGGER on_create_or_update_employee_check_department_foundation_date_below_employment_date
  BEFORE INSERT OR UPDATE
  ON employees
  FOR EACH ROW
EXECUTE PROCEDURE check_department_foundation_below_hiring_date();