DROP TRIGGER on_create_or_update_employee_check_born_date_below_employment_date ON employees;
DROP TRIGGER on_create_or_update_employee_check_department_foundation_date_below_employment_date ON employees;
DROP FUNCTION check_employee_born_date();
DROP FUNCTION check_department_foundation_below_hiring_date();

DROP TABLE shipments;
DROP TABLE shipment_orders;
DROP TABLE lorries;
DROP TABLE cars;
DROP TABLE driver_categories;
DROP TABLE drivers;
DROP TABLE employee_courses;
DROP TABLE courses;
DROP TABLE employees;
DROP TABLE departments;
DROP TABLE mans;

DROP SEQUENCE jpa_sequence;