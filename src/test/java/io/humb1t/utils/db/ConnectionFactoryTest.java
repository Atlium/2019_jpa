package io.humb1t.utils.db;

import org.junit.AfterClass;
import org.junit.Test;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertTrue;

public class ConnectionFactoryTest {

    @AfterClass
    public static void closeFactory() {
        JpaUtils.closeFactory();
    }

    @Test
    public void checkConnection() {
        EntityManager entityManager = JpaUtils.getEntityManager();
        assertTrue(entityManager.isOpen());
    }
}