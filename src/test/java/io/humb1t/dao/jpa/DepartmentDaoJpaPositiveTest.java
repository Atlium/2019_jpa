package io.humb1t.dao.jpa;

import io.humb1t.dao.DepartmentDao;
import io.humb1t.dao.EmployeeDao;
import io.humb1t.dao.ManDao;
import io.humb1t.domain.employee.DepartmentEntity;
import io.humb1t.utils.db.JpaUtils;
import io.humb1t.utils.db.RequestWrapper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.text.ParseException;
import java.util.List;

import static io.humb1t.dao.DbConfig.*;
import static io.humb1t.dao.jpa.EntityHelper.*;
import static org.junit.Assert.*;

public class DepartmentDaoJpaPositiveTest {
    private static DepartmentDao departmentDao;
    private static ManDao manDao;
    private static EmployeeDao employeeDao;

    @BeforeClass
    public static void beforeClass() {
        departmentDao = new DepartmentDaoJpa();
        manDao = new ManDaoJpa();
        employeeDao = new EmployeeDaoJpa();
        RequestWrapper.wrapEntityManagerRequest(JpaUtils.getEntityManager(), em -> em.createNativeQuery(CREATE_JPA_SCRIPT).executeUpdate());
    }

    @AfterClass
    public static void afterClass() {
        RequestWrapper.wrapEntityManagerRequest(JpaUtils.getEntityManager(), em -> em.createNativeQuery(DROP_JPA_SCRIPT).executeUpdate());
    }

    @After
    public void cleanDb() {
        RequestWrapper.wrapEntityManagerRequest(JpaUtils.getEntityManager(), em -> em.createNativeQuery(DELETE_SCRIPT).executeUpdate());
    }

    @Test
    public void createNewDepartment() throws ParseException {
        DepartmentEntity departmentEntity = createTemplateDepartment();
        assertEquals(departmentEntity, departmentDao.create(departmentEntity));
    }

    @Test
    public void removeDepartmentReturnTrue() throws ParseException {
        DepartmentEntity departmentEntity = departmentDao.create(createTemplateDepartment());
        assertTrue(departmentDao.remove(departmentEntity.getId()));
    }

    @Test
    public void removeDepartmentReturnFalse() {
        assertFalse(departmentDao.remove(-1L));
    }

    @Test
    public void update() throws ParseException {
        String expectedName = "Sales";
        DepartmentEntity departmentEntity = departmentDao.create(createTemplateDepartment());
        departmentEntity.setDeptName(expectedName);
        DepartmentEntity updatedDepartment = departmentDao.update(departmentEntity);
        assertEquals(expectedName, updatedDepartment.getDeptName());
    }

    @Test
    public void findAll() throws ParseException {
        departmentDao.create(createTemplateDepartment());
        List<DepartmentEntity> all = departmentDao.findAll();
        assertFalse(all.isEmpty());
    }

    @Test
    public void find() throws ParseException {
        DepartmentEntity created = departmentDao.create(createTemplateDepartment());
        DepartmentEntity founded = departmentDao.find(created.getId());
        assertEquals(created, founded);
    }

    @Test
    public void findMostPopular() throws ParseException {
        DepartmentEntity departmentPopular = departmentDao.create(createTemplateDepartment());
        DepartmentEntity departmentNonPopular = departmentDao.create(createTemplateDepartment());
        employeeDao.create(createTemplateEmployee(manDao.create(createTemplateMan()), departmentPopular));
        employeeDao.create(createTemplateEmployee(manDao.create(createTemplateMan()), departmentPopular));
        employeeDao.create(createTemplateEmployee(manDao.create(createTemplateMan()), departmentNonPopular));
        List<DepartmentEntity> mostPopularDepartments = departmentDao.findMostPopular();
        assertEquals(departmentPopular, mostPopularDepartments.get(0));
    }
}