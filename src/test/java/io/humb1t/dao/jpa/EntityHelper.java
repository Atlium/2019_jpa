package io.humb1t.dao.jpa;

import io.humb1t.domain.car.LorryEntity;
import io.humb1t.domain.employee.*;
import io.humb1t.domain.shipment.ShipmentEntity;
import io.humb1t.domain.shipment.ShipmentId;
import io.humb1t.domain.shipment.ShipmentOrderEntity;
import io.humb1t.utils.date.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class EntityHelper {
    private EntityHelper() {
        //util
    }

    private static int currentIndex = 0;

    private static int nextIndex() {
        return currentIndex++;
    }

    public static ManEntity createTemplateMan() throws ParseException {
        int next = nextIndex();
        return new ManEntity(null, "qwe" + next, "rty" + next,
                new SimpleDateFormat("dd.mm.yyyy").parse("10.10.1990"));
    }

    public static DepartmentEntity createTemplateDepartment() throws ParseException {
        return new DepartmentEntity(null, "Testing" + nextIndex(),
                new SimpleDateFormat("dd.mm.yyyy").parse("15.12.1979"));
    }

    public static EmployeeEntity createTemplateEmployee(ManEntity man, DepartmentEntity department) {
        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeEntity.setMan(man);
        employeeEntity.setDepartment(department);
        employeeEntity.setEmploymentDate(DateUtils.getWithAddedYears(man.getBirthDate(), 18));
        return employeeEntity;
    }

    public static DriverEntity createTemplateDriver(ManEntity man, DepartmentEntity department, List<DriverCategory> categories) {
        DriverEntity driverEntity = new DriverEntity();
        driverEntity.setMan(man);
        driverEntity.setDepartment(department);
        driverEntity.setEmploymentDate(DateUtils.getWithAddedYears(man.getBirthDate(), 18));
        driverEntity.setCategories(categories);
        return driverEntity;
    }

    public static LorryEntity createTemplateLorry() {
        LorryEntity lorryEntity = new LorryEntity();
        lorryEntity.setCarrying(20);
        lorryEntity.setMaxSpeed(110);
        lorryEntity.setModel("Gazel");
        lorryEntity.setProducer("Gaz");
        return lorryEntity;
    }

    public static ShipmentOrderEntity createTemplateShipmentOrder() throws ParseException {
        ShipmentOrderEntity shipmentOrderEntity = new ShipmentOrderEntity();
        shipmentOrderEntity.setAddress("Pushkin street 11 A");
        shipmentOrderEntity.setShipmentDate(new SimpleDateFormat("dd.mm.yyyy").parse("15.12.2018"));
        return shipmentOrderEntity;
    }

    public static ShipmentEntity createTemplateShipment(ShipmentOrderEntity shipmentOrderEntity, DriverEntity driverEntity, LorryEntity lorryEntity) {
        ShipmentEntity shipmentEntity = new ShipmentEntity();
        shipmentEntity.setDriver(driverEntity);
        shipmentEntity.setLorry(lorryEntity);
        shipmentEntity.setShipmentOrder(shipmentOrderEntity);
        ShipmentId shipmentId = shipmentEntity.getShipmentId();
        shipmentId.setDriverId(driverEntity.getId());
        shipmentId.setLorryId(lorryEntity.getId());
        shipmentId.setShipmentOrderId(shipmentOrderEntity.getId());
        return shipmentEntity;
    }
}
