package io.humb1t.dao.jpa;

import io.humb1t.dao.CourseDao;
import io.humb1t.dao.DepartmentDao;
import io.humb1t.dao.EmployeeDao;
import io.humb1t.dao.ManDao;
import io.humb1t.domain.activity.CourseEntity;
import io.humb1t.domain.employee.DepartmentEntity;
import io.humb1t.domain.employee.EmployeeEntity;
import io.humb1t.domain.employee.ManEntity;
import io.humb1t.utils.date.DateUtils;
import io.humb1t.utils.db.JpaUtils;
import io.humb1t.utils.db.RequestWrapper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import static io.humb1t.dao.DbConfig.*;
import static io.humb1t.dao.jpa.EntityHelper.*;
import static org.junit.Assert.*;

public class EmployeeDaoJpaPositiveTest {
    private static ManDao manDao;
    private static DepartmentDao departmentDao;
    private static EmployeeDao employeeDao;
    private static CourseDao courseDao;

    @BeforeClass
    public static void beforeClass() {
        manDao = new ManDaoJpa();
        departmentDao = new DepartmentDaoJpa();
        employeeDao = new EmployeeDaoJpa();
        courseDao = new CourseDaoJpa();
        RequestWrapper.wrapEntityManagerRequest(JpaUtils.getEntityManager(), em -> em.createNativeQuery(CREATE_JPA_SCRIPT).executeUpdate());
    }

    @AfterClass
    public static void afterClass() {
        RequestWrapper.wrapEntityManagerRequest(JpaUtils.getEntityManager(), em -> em.createNativeQuery(DROP_JPA_SCRIPT).executeUpdate());
    }

    @After
    public void cleanDb() {
        RequestWrapper.wrapEntityManagerRequest(JpaUtils.getEntityManager(), em -> em.createNativeQuery(DELETE_SCRIPT).executeUpdate());
    }

    @Test
    public void createNewEmployeeWithNewManAndDept() throws ParseException {
        EmployeeEntity employee = createTemplateEmployee(createTemplateMan(), createTemplateDepartment());
        assertEquals(employee, employeeDao.create(employee));
    }

    @Test
    public void createNewEmployeeWithExistedManAndDept() throws ParseException {
        ManEntity manEntity = manDao.create(createTemplateMan());
        DepartmentEntity departmentEntity = departmentDao.create(createTemplateDepartment());
        EmployeeEntity employee = createTemplateEmployee(manEntity, departmentEntity);
        assertEquals(employee, employeeDao.create(employee));
    }

    @Test
    public void removeEmployeeReturnTrue() throws ParseException {
        EmployeeEntity employeeEntity = employeeDao.create(createTemplateEmployee(createTemplateMan(), createTemplateDepartment()));
        assertTrue(employeeDao.remove(employeeEntity.getId()));
    }

    @Test
    public void removeEmployeeReturnFalse() {
        assertFalse(employeeDao.remove(-1L));
    }

    @Test
    public void updateEmployee() throws ParseException {
        Date employmentDate = new Date();
        Date expectedEmploymentDate = DateUtils.getWithAddedYears(employmentDate, -1);
        EmployeeEntity employee = createTemplateEmployee(createTemplateMan(), createTemplateDepartment());
        employee.setEmploymentDate(employmentDate);
        employee = employeeDao.create(employee);
        employee.setEmploymentDate(expectedEmploymentDate);
        EmployeeEntity updated = employeeDao.update(employee);
        assertEquals(expectedEmploymentDate, updated.getEmploymentDate());
    }

    @Test
    public void findAllEmployees() throws ParseException {
        int expectedSize = 4;
        for (int i = 0; i < expectedSize; i++) {
            employeeDao.create(createTemplateEmployee(createTemplateMan(), createTemplateDepartment()));
        }
        assertEquals(expectedSize, employeeDao.findAll().size());
    }

    @Test
    public void addCoursesToEmployee() throws ParseException {
        int expectedCoursesCount = 1;
        CourseEntity courseEntity = courseDao.create(new CourseEntity("Java", 2, new HashSet<>()));
        EmployeeEntity created = employeeDao.create(createTemplateEmployee(createTemplateMan(), createTemplateDepartment()));
        created.getCourses().add(courseEntity);
        courseEntity.getEmployees().add(created);
        employeeDao.update(created);
        assertEquals(expectedCoursesCount, created.getCourses().size());
    }

    @Test
    public void findEmployee() throws ParseException {
        EmployeeEntity created = employeeDao.create(createTemplateEmployee(createTemplateMan(), createTemplateDepartment()));
        EmployeeEntity founded = employeeDao.find(created.getId());
        assertEquals(created, founded);
    }

    @Test
    public void findAllByDepartmentIdAndWorkExperience() throws ParseException {
        DepartmentEntity department = departmentDao.create(createTemplateDepartment());
        DepartmentEntity departmentWithoutEmployees = departmentDao.create(createTemplateDepartment());
        int expectedSize = 4;
        for (int i = 0; i < expectedSize; i++) {
            employeeDao.create(createTemplateEmployee(createTemplateMan(), department));
        }
        List<EmployeeEntity> allByDepartmentIdAndWorkExperience = employeeDao.findAllByDepartmentIdAndWorkExperience(department.getId(), 5);
        assertEquals(expectedSize, allByDepartmentIdAndWorkExperience.size());
    }
}