package io.humb1t.dao.jpa;

import io.humb1t.dao.*;
import io.humb1t.domain.car.LorryEntity;
import io.humb1t.domain.employee.*;
import io.humb1t.domain.shipment.ShipmentEntity;
import io.humb1t.domain.shipment.ShipmentId;
import io.humb1t.domain.shipment.ShipmentOrderEntity;
import io.humb1t.utils.date.DateUtils;
import io.humb1t.utils.db.JpaUtils;
import io.humb1t.utils.db.RequestWrapper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static io.humb1t.dao.DbConfig.*;
import static io.humb1t.dao.jpa.EntityHelper.*;
import static org.junit.Assert.*;

public class AllTypeMethodsTest {
    private static DepartmentDao departmentDao;
    private static ManDao manDao;
    private static EmployeeDao employeeDao;
    private static LorryDao lorryDao;
    private static DriverDao driverDao;
    private static ShipmentOrderDao shipmentOrderDao;
    private static ShipmentDao shipmentDao;

    @BeforeClass
    public static void beforeClass() {
        departmentDao = new DepartmentDaoJpa();
        manDao = new ManDaoJpa();
        employeeDao = new EmployeeDaoJpa();
        lorryDao = new LorryDaoJpa();
        driverDao = new DriverDaoJpa();
        shipmentOrderDao = new ShipmentOrderDaoJpa();
        shipmentDao = new ShipmentDaoJpa();
        RequestWrapper.wrapEntityManagerRequest(JpaUtils.getEntityManager(), em -> em.createNativeQuery(CREATE_JPA_SCRIPT).executeUpdate());
    }

    @AfterClass
    public static void afterClass() {
        RequestWrapper.wrapEntityManagerRequest(JpaUtils.getEntityManager(), em -> em.createNativeQuery(DROP_JPA_SCRIPT).executeUpdate());
    }

    @After
    public void cleanDb() {
        RequestWrapper.wrapEntityManagerRequest(JpaUtils.getEntityManager(), em -> em.createNativeQuery(DELETE_SCRIPT).executeUpdate());
    }

    @Test
    public void findDepartmentIdsByManBirthdays() throws ParseException {
        Date fromDate = new SimpleDateFormat("dd.mm.yyyy").parse("10.10.1971");
        Date toDate = new SimpleDateFormat("dd.mm.yyyy").parse("10.10.2018");
        DepartmentEntity departmentPopular = departmentDao.create(createTemplateDepartment());
        DepartmentEntity departmentNonPopular = departmentDao.create(createTemplateDepartment());
        employeeDao.create(createTemplateEmployee(manDao.create(createTemplateMan()), departmentPopular));
        employeeDao.create(createTemplateEmployee(manDao.create(createTemplateMan()), departmentPopular));
        employeeDao.create(createTemplateEmployee(manDao.create(createTemplateMan()), departmentPopular));
        employeeDao.create(createTemplateEmployee(manDao.create(createTemplateMan()), departmentNonPopular));

        List<DepartmentEntity> departmentsNative = departmentDao.findByManBirthdaysNative(fromDate, toDate);
        List<DepartmentEntity> departmentsJpql = departmentDao.findByManBirthdaysJpql(fromDate, toDate);
        List<DepartmentEntity> departmentsNamed = departmentDao.findByManBirthdaysNamed(fromDate, toDate);
        List<DepartmentEntity> departmentsCriteria = departmentDao.findByManBirthdaysCriteria(fromDate, toDate);

        assertEquals(departmentPopular, departmentsNative.get(0));
        assertEquals(departmentPopular, departmentsJpql.get(0));
        assertEquals(departmentPopular, departmentsNamed.get(0));
        assertEquals(departmentPopular, departmentsCriteria.get(0));
    }

    @Test
    public void findOrdersWithShipmentsCount() throws ParseException {
        Long shipmentCount = 2L;
        ShipmentOrderEntity orderMultiplyShipment = shipmentOrderDao.create(createTemplateShipmentOrder());
        DepartmentEntity department = departmentDao.create(createTemplateDepartment());
        List<DriverCategory> driverCategories = new ArrayList<>(Collections.singleton(DriverCategory.B));

        DriverEntity driver1 = driverDao.create(createTemplateDriver(createTemplateMan(), department, driverCategories));
        LorryEntity lorry1 = lorryDao.create(createTemplateLorry());
        shipmentDao.create(createTemplateShipment(orderMultiplyShipment, driver1, lorry1));
        DriverEntity driver2 = driverDao.create(createTemplateDriver(createTemplateMan(), department, driverCategories));
        LorryEntity lorry2 = lorryDao.create(createTemplateLorry());
        shipmentDao.create(createTemplateShipment(orderMultiplyShipment, driver2, lorry2));

        ShipmentOrderEntity orderSingleShipment = shipmentOrderDao.create(createTemplateShipmentOrder());
        DriverEntity driver3 = driverDao.create(createTemplateDriver(createTemplateMan(), department, driverCategories));
        LorryEntity lorry3 = lorryDao.create(createTemplateLorry());
        shipmentDao.create(createTemplateShipment(orderSingleShipment, driver3, lorry3));

        List<ShipmentOrderEntity> ordersNative = shipmentOrderDao.findOrdersWithShipmentsCountNative(shipmentCount);
        List<ShipmentOrderEntity> ordersJpql = shipmentOrderDao.findOrdersWithShipmentsCountJpql(shipmentCount);
        List<ShipmentOrderEntity> ordersNamed = shipmentOrderDao.findOrdersWithShipmentsCountNamed(shipmentCount);
        List<ShipmentOrderEntity> ordersCriteria = shipmentOrderDao.findOrdersWithShipmentsCountCriteria(shipmentCount);

        assertEquals(orderMultiplyShipment, ordersNative.get(0));
        assertEquals(orderMultiplyShipment, ordersJpql.get(0));
        assertEquals(orderMultiplyShipment, ordersNamed.get(0));
        assertEquals(orderMultiplyShipment, ordersCriteria.get(0));
    }
}