package io.humb1t.dao.jpa;

import io.humb1t.dao.DriverDao;
import io.humb1t.dao.LorryDao;
import io.humb1t.dao.ShipmentDao;
import io.humb1t.dao.ShipmentOrderDao;
import io.humb1t.domain.car.LorryEntity;
import io.humb1t.domain.employee.DriverCategory;
import io.humb1t.domain.employee.DriverEntity;
import io.humb1t.domain.shipment.ShipmentEntity;
import io.humb1t.domain.shipment.ShipmentId;
import io.humb1t.domain.shipment.ShipmentOrderEntity;
import io.humb1t.utils.db.JpaUtils;
import io.humb1t.utils.db.RequestWrapper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static io.humb1t.dao.DbConfig.*;
import static io.humb1t.dao.jpa.EntityHelper.*;
import static org.junit.Assert.*;

public class ShipmentDaoJpaPositiveTest {
    private static LorryDao lorryDao;
    private static DriverDao driverDao;
    private static ShipmentOrderDao shipmentOrderDao;
    private static ShipmentDao shipmentDao;

    @BeforeClass
    public static void beforeClass() {
        lorryDao = new LorryDaoJpa();
        driverDao = new DriverDaoJpa();
        shipmentOrderDao = new ShipmentOrderDaoJpa();
        shipmentDao = new ShipmentDaoJpa();
        RequestWrapper.wrapEntityManagerRequest(JpaUtils.getEntityManager(), em -> em.createNativeQuery(CREATE_JPA_SCRIPT).executeUpdate());
    }

    @AfterClass
    public static void afterClass() {
        RequestWrapper.wrapEntityManagerRequest(JpaUtils.getEntityManager(), em -> em.createNativeQuery(DROP_JPA_SCRIPT).executeUpdate());
    }

    @After
    public void cleanDb() {
        RequestWrapper.wrapEntityManagerRequest(JpaUtils.getEntityManager(), em -> em.createNativeQuery(DELETE_SCRIPT).executeUpdate());
    }

    @Test
    public void createNewShipment() throws ParseException {
        DriverEntity driver = driverDao.create(createTemplateDriver(createTemplateMan(), createTemplateDepartment(), new ArrayList<>(Collections.singleton(DriverCategory.B))));
        ShipmentOrderEntity shipmentOrder = shipmentOrderDao.create(createTemplateShipmentOrder());
        LorryEntity lorry = lorryDao.create(createTemplateLorry());
        ShipmentEntity shipmentEntity = createTemplateShipment(shipmentOrder, driver, lorry);
        assertEquals(shipmentEntity, shipmentDao.create(shipmentEntity));
    }

    @Test
    public void removeLorryReturnTrue() throws ParseException {
        DriverEntity driver = driverDao.create(createTemplateDriver(createTemplateMan(), createTemplateDepartment(), new ArrayList<>(Collections.singleton(DriverCategory.B))));
        ShipmentOrderEntity shipmentOrder = shipmentOrderDao.create(createTemplateShipmentOrder());
        LorryEntity lorry = lorryDao.create(createTemplateLorry());
        ShipmentEntity shipmentEntity = shipmentDao.create(createTemplateShipment(shipmentOrder, driver, lorry));
        assertTrue(shipmentDao.remove(shipmentEntity.getShipmentId()));
    }

    @Test
    public void removeLorryReturnFalse() {
        assertFalse(shipmentDao.remove(new ShipmentId()));
    }

    @Test
    public void update() throws ParseException {
        DriverEntity driver = driverDao.create(createTemplateDriver(createTemplateMan(), createTemplateDepartment(), new ArrayList<>(Collections.singleton(DriverCategory.A))));
        DriverEntity otherDriver = driverDao.create(createTemplateDriver(createTemplateMan(), createTemplateDepartment(), new ArrayList<>(Collections.singleton(DriverCategory.B))));
        ShipmentOrderEntity shipmentOrder = shipmentOrderDao.create(createTemplateShipmentOrder());
        LorryEntity lorry = lorryDao.create(createTemplateLorry());
        ShipmentEntity shipmentEntity = shipmentDao.create(createTemplateShipment(shipmentOrder, driver, lorry));
        shipmentEntity.setDriver(otherDriver);
        ShipmentEntity update = shipmentDao.update(shipmentEntity);
        assertEquals(otherDriver, update.getDriver());
    }

    @Test
    public void findAll() throws ParseException {
        DriverEntity driver = driverDao.create(createTemplateDriver(createTemplateMan(), createTemplateDepartment(), new ArrayList<>(Collections.singleton(DriverCategory.B))));
        ShipmentOrderEntity shipmentOrder = shipmentOrderDao.create(createTemplateShipmentOrder());
        LorryEntity lorry = lorryDao.create(createTemplateLorry());
        shipmentDao.create(createTemplateShipment(shipmentOrder, driver, lorry));
        List<ShipmentEntity> all = shipmentDao.findAll();
        assertFalse(all.isEmpty());
    }

    @Test
    public void find() throws ParseException {
        DriverEntity driver = driverDao.create(createTemplateDriver(createTemplateMan(), createTemplateDepartment(), new ArrayList<>(Collections.singleton(DriverCategory.B))));
        ShipmentOrderEntity shipmentOrder = shipmentOrderDao.create(createTemplateShipmentOrder());
        LorryEntity lorry = lorryDao.create(createTemplateLorry());
        ShipmentEntity created = shipmentDao.create(createTemplateShipment(shipmentOrder, driver, lorry));
        ShipmentEntity founded = shipmentDao.find(created.getShipmentId());
        assertEquals(created, founded);
    }
}