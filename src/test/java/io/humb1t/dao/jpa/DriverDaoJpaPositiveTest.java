package io.humb1t.dao.jpa;

import io.humb1t.dao.DepartmentDao;
import io.humb1t.dao.DriverDao;
import io.humb1t.dao.ManDao;
import io.humb1t.domain.activity.CourseEntity;
import io.humb1t.domain.employee.DepartmentEntity;
import io.humb1t.domain.employee.DriverCategory;
import io.humb1t.domain.employee.DriverEntity;
import io.humb1t.domain.employee.EmployeeEntity;
import io.humb1t.utils.date.DateUtils;
import io.humb1t.utils.db.JpaUtils;
import io.humb1t.utils.db.RequestWrapper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.text.ParseException;
import java.util.*;

import static io.humb1t.dao.DbConfig.*;
import static io.humb1t.dao.jpa.EntityHelper.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DriverDaoJpaPositiveTest {
    private static ManDao manDao;
    private static DepartmentDao departmentDao;
    private static DriverDao driverDao;

    @BeforeClass
    public static void beforeClass() {
        manDao = new ManDaoJpa();
        departmentDao = new DepartmentDaoJpa();
        driverDao = new DriverDaoJpa();
        RequestWrapper.wrapEntityManagerRequest(JpaUtils.getEntityManager(), em -> em.createNativeQuery(CREATE_JPA_SCRIPT).executeUpdate());
    }

    @AfterClass
    public static void afterClass() {
        RequestWrapper.wrapEntityManagerRequest(JpaUtils.getEntityManager(), em -> em.createNativeQuery(DROP_JPA_SCRIPT).executeUpdate());
    }

    @After
    public void cleanDb() {
        RequestWrapper.wrapEntityManagerRequest(JpaUtils.getEntityManager(), em -> em.createNativeQuery(DELETE_SCRIPT).executeUpdate());
    }

    @Test
    public void createNewDriver() throws ParseException {
        DriverEntity driver = createTemplateDriver(createTemplateMan(), createTemplateDepartment(), new ArrayList<>(Collections.singleton(DriverCategory.B)));
        DriverEntity driverEntity = driverDao.create(driver);
        assertEquals(driver, driverEntity);
    }

    @Test
    public void createNewDriverWithExistedManAndDept() throws ParseException {
        DriverEntity driver = createTemplateDriver(manDao.create(createTemplateMan()),
                departmentDao.create(createTemplateDepartment()), new ArrayList<>(Collections.singleton(DriverCategory.B)));
        DriverEntity driverEntity = driverDao.create(driver);
        assertEquals(driver, driverEntity);
    }

    @Test
    public void removeDriverReturnTrue() throws ParseException {
        DriverEntity driverEntity = driverDao.create(createTemplateDriver(createTemplateMan(), createTemplateDepartment(), new ArrayList<>(Collections.singleton(DriverCategory.B))));
        assertTrue(driverDao.remove(driverEntity.getId()));
    }

    @Test
    public void removeEmployeeReturnFalse() {
        assertFalse(driverDao.remove(-1L));
    }

    @Test
    public void updateDriver() throws ParseException {
        int expectedCategoryCount = 2;
        DriverEntity driver = createTemplateDriver(createTemplateMan(), createTemplateDepartment(), new ArrayList<>());
        driver = driverDao.create(driver);
        driver.getCategories().add(DriverCategory.A);
        driver.getCategories().add(DriverCategory.D);
        DriverEntity updated = driverDao.update(driver);
        assertEquals(expectedCategoryCount, updated.getCategories().size());
    }

    @Test
    public void findAllDrivers() throws ParseException {
        int expectedSize = 4;
        for (int i = 0; i < expectedSize; i++) {
            driverDao.create(createTemplateDriver(createTemplateMan(), createTemplateDepartment(), new ArrayList<>(new ArrayList<>(Collections.singleton(DriverCategory.B)))));
        }
        assertEquals(expectedSize, driverDao.findAll().size());
    }

    @Test
    public void findDriver() throws ParseException {
        EmployeeEntity created = driverDao.create(createTemplateDriver(createTemplateMan(), createTemplateDepartment(), new ArrayList<>(new ArrayList<>(Collections.singleton(DriverCategory.B)))));
        EmployeeEntity founded = driverDao.find(created.getId());
        assertEquals(created, founded);
    }

    @Test
    public void findOldestByDepartmentId() throws ParseException {
        DepartmentEntity department = departmentDao.create(createTemplateDepartment());
        List<DriverCategory> categories = new ArrayList<>(Collections.singleton(DriverCategory.B));
        int count = 4;
        for (int i = 0; i < count; i++) {
            DriverEntity driverEntity = createTemplateDriver(createTemplateMan(), department, categories);
            driverEntity.getMan().setBirthDate(DateUtils.getWithAddedYears(driverEntity.getMan().getBirthDate(), -1));
            driverDao.create(driverEntity);

        }
        for (int i = 0; i < count; i++) {
            DriverEntity driverEntity = createTemplateDriver(createTemplateMan(), department, categories);
            driverEntity.getMan().setBirthDate(DateUtils.getWithAddedYears(driverEntity.getMan().getBirthDate(), +2));
            driverDao.create(driverEntity);
        }
        for (int i = 0; i < count; i++) {
            driverDao.create(createTemplateDriver(createTemplateMan(), department, categories));
        }
        DriverEntity oldestDriver = driverDao.create(createTemplateDriver(createTemplateMan(), department, categories));
        oldestDriver.getMan().setBirthDate(DateUtils.getWithAddedYears(oldestDriver.getMan().getBirthDate(), -3));
        manDao.update(oldestDriver.getMan());
        assertEquals(oldestDriver, driverDao.findOldestByDepartmentId(department.getId()));
    }
}