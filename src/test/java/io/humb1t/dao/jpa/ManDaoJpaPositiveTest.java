package io.humb1t.dao.jpa;

import io.humb1t.dao.ManDao;
import io.humb1t.domain.employee.ManEntity;
import io.humb1t.utils.db.JpaUtils;
import io.humb1t.utils.db.RequestWrapper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.text.ParseException;
import java.util.List;

import static io.humb1t.dao.DbConfig.*;
import static org.junit.Assert.*;
import static io.humb1t.dao.jpa.EntityHelper.createTemplateMan;

public class ManDaoJpaPositiveTest {
    private static ManDao manDao;

    @BeforeClass
    public static void beforeClass() {
        manDao = new ManDaoJpa();
        RequestWrapper.wrapEntityManagerRequest(JpaUtils.getEntityManager(), em -> em.createNativeQuery(CREATE_JPA_SCRIPT).executeUpdate());
    }

    @AfterClass
    public static void afterClass() {
        RequestWrapper.wrapEntityManagerRequest(JpaUtils.getEntityManager(), em -> em.createNativeQuery(DROP_JPA_SCRIPT).executeUpdate());
        JpaUtils.closeFactory();
    }

    @After
    public void cleanDb() {
        RequestWrapper.wrapEntityManagerRequest(JpaUtils.getEntityManager(), em -> em.createNativeQuery(DELETE_SCRIPT).executeUpdate());
    }

    @Test
    public void createNewMan() throws ParseException {
        ManEntity manEntity = createTemplateMan();
        assertEquals(manEntity, manDao.create(manEntity));
    }

    @Test
    public void removeManReturnTrue() throws ParseException {
        ManEntity manEntity = manDao.create(createTemplateMan());
        assertTrue(manDao.remove(manEntity.getId()));
    }

    @Test
    public void removeManReturnFalse() {
        assertFalse(manDao.remove(-1L));
    }

    @Test
    public void updateMan() throws ParseException {
        String expectedName = "poi123";
        ManEntity manEntity = manDao.create(createTemplateMan());
        manEntity.setFirstName(expectedName);
        ManEntity updatedMan = manDao.update(manEntity);
        assertEquals(expectedName, updatedMan.getFirstName());
    }

    @Test
    public void findAllMan() throws ParseException {
        manDao.create(createTemplateMan());
        List<ManEntity> all = manDao.findAll();
        assertFalse(all.isEmpty());
    }

    @Test
    public void findMan() throws ParseException {
        ManEntity created = manDao.create(createTemplateMan());
        ManEntity founded = manDao.find(created.getId());
        assertEquals(created, founded);
    }

    @Test
    public void findAllUnemployed() throws ParseException {
        int expectedSize = 2;
        manDao.create(createTemplateMan());
        manDao.create(createTemplateMan());
        List<ManEntity> allUnemployed = manDao.findAllUnemployed();
        assertEquals(expectedSize, allUnemployed.size());
    }
}