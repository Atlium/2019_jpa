package io.humb1t.dao.jpa;

import io.humb1t.dao.LorryDao;
import io.humb1t.domain.car.LorryEntity;
import io.humb1t.utils.db.JpaUtils;
import io.humb1t.utils.db.RequestWrapper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static io.humb1t.dao.DbConfig.*;
import static io.humb1t.dao.jpa.EntityHelper.createTemplateLorry;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LorryDaoJpaPositiveTest {
    private static LorryDao lorryDao;

    @BeforeClass
    public static void beforeClass() {
        lorryDao = new LorryDaoJpa();
        RequestWrapper.wrapEntityManagerRequest(JpaUtils.getEntityManager(), em -> em.createNativeQuery(CREATE_JPA_SCRIPT).executeUpdate());
    }

    @AfterClass
    public static void afterClass() {
        RequestWrapper.wrapEntityManagerRequest(JpaUtils.getEntityManager(), em -> em.createNativeQuery(DROP_JPA_SCRIPT).executeUpdate());
    }

    @After
    public void cleanDb() {
        RequestWrapper.wrapEntityManagerRequest(JpaUtils.getEntityManager(), em -> em.createNativeQuery(DELETE_SCRIPT).executeUpdate());
    }

    @Test
    public void createNewLorry() {
        LorryEntity lorryEntity = createTemplateLorry();
        assertEquals(lorryEntity, lorryDao.create(lorryEntity));
    }

    @Test
    public void removeLorryReturnTrue() {
        LorryEntity lorryEntity = lorryDao.create(createTemplateLorry());
        assertTrue(lorryDao.remove(lorryEntity.getId()));
    }

    @Test
    public void removeLorryReturnFalse() {
        assertFalse(lorryDao.remove(-1L));
    }

    @Test
    public void update() {
        Integer expectedMaxSpeed = 90;
        LorryEntity lorryEntity = lorryDao.create(createTemplateLorry());
        lorryEntity.setMaxSpeed(expectedMaxSpeed);
        LorryEntity updated = lorryDao.update(lorryEntity);
        assertEquals(expectedMaxSpeed, updated.getMaxSpeed());
    }

    @Test
    public void findAll() {
        lorryDao.create(createTemplateLorry());
        List<LorryEntity> all = lorryDao.findAll();
        assertFalse(all.isEmpty());
    }

    @Test
    public void find() {
        LorryEntity created = lorryDao.create(createTemplateLorry());
        LorryEntity founded = lorryDao.find(created.getId());
        assertEquals(created, founded);
    }
}