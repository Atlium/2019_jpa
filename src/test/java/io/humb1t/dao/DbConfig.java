package io.humb1t.dao;

import io.humb1t.utils.file.FileFactory;
import io.humb1t.utils.file.FileSource;
import io.humb1t.utils.file.FileUtils;

public class DbConfig {

    private static final FileFactory FILE_FACTORY;

    private static final String CREATE_JPA_SCRIPT_FILE = "create_jpa.sql";
    private static final String DROP_JPA_SCRIPT_FILE = "drop_jpa.sql";
    private static final String DELETE_SCRIPT_FILE = "delete.sql";

    public static final String CREATE_JPA_SCRIPT;
    public static final String DROP_JPA_SCRIPT;
    public static final String DELETE_SCRIPT;

    private DbConfig() {
        //constants
    }

    static {
        FILE_FACTORY = new FileFactory();

        CREATE_JPA_SCRIPT = FileUtils.getTextFromFile(FILE_FACTORY.getFile(FileSource.RESOURCE, CREATE_JPA_SCRIPT_FILE));
        DROP_JPA_SCRIPT = FileUtils.getTextFromFile(FILE_FACTORY.getFile(FileSource.RESOURCE, DROP_JPA_SCRIPT_FILE));
        DELETE_SCRIPT = FileUtils.getTextFromFile(FILE_FACTORY.getFile(FileSource.RESOURCE, DELETE_SCRIPT_FILE));
    }

}
