package io.humb1t.suit;

import io.humb1t.dao.jpa.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({DepartmentDaoJpaPositiveTest.class, ManDaoJpaPositiveTest.class,
        EmployeeDaoJpaPositiveTest.class, DriverDaoJpaPositiveTest.class,
        LorryDaoJpaPositiveTest.class, ShipmentDaoJpaPositiveTest.class})
public class DaoTestSuite {
}
