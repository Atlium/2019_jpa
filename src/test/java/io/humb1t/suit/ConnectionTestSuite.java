package io.humb1t.suit;

import io.humb1t.utils.db.ConnectionFactoryTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses(ConnectionFactoryTest.class)
public class ConnectionTestSuite {
}
