package io.humb1t.suit;

import io.humb1t.dao.jpa.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses(AllTypeMethodsTest.class)
public class AllTypeMethodsTestSuite {
}
